<?php 
define("BASE_PATH", dirname ( __FILE__ ));
define("LAYOUT_PATH", BASE_PATH."/layout/");
define("VIEW_PATH", BASE_PATH."/views/");
define("LIB_PATH", BASE_PATH. "/lib/");
define("CURRENCY_SYMBOL", "R");
define("DISTANCE_UNIT", "KM");
define("SPACER", "&nbsp;&nbsp;&nbsp;");

$base_url = $_SERVER['SCRIPT_NAME'];
$base_url = str_replace("index.php", "", $base_url);
$base_url = str_replace("ajax.php", "", $base_url);
define("BASE_URL", $base_url);

require_once(LIB_PATH."Database.php");

 function escapeJavaScriptText($string)
    {
        return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
    }
function get_layout($tpl_name){

    require_once(LAYOUT_PATH."{$tpl_name}.php");

}

function get_cars(){
    $db = new Database();
    $make = isset($_POST['make']) ? (int)$_POST['make'] : "NULL";
    $model = isset($_POST['model']) ? (int)$_POST['model'] : "NULL";
    $make = $make != -1 ? $make : "NULL";
    $model = $model != -1 ? $model : "NULL";

    $price_from = isset($_POST['price_from']) ? (float)$_POST['price_from'] : 1000;
    $price_to = isset($_POST['price_to']) ? (float)$_POST['price_to'] : 1000000;
    $cars = $db->query(
        "CALL search_cars({$make}, {$model}, {$price_from},{$price_to} )");
    
    if($cars){
        return $db->fetch_many($cars);

    }else{

        return [];
    }
    $db->close();

}
function get_models_by_make($make_id=null){
    if($make_id!=null){
        $db = new Database();
        $sql = "select id, name from models where make_id='{$make_id}'";
        $models = $db->query($sql);
        if($models){
            $models = $db->fetch_many($models);
            print json_encode($models);

        }else{
             print json_encode([]);
        }
    }else{
        print json_encode([]);
    }
    $db->close();

}
