
<section id="section4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Contact Us</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="row form-group">
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" required="">
                    </div>
                  
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" required="">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-5">
                        <input type="email" class="form-control" name="email" placeholder="Email" required="">
                    </div>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" name="phone" placeholder="Phone" required="">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-10">
                        <strong>Message:</strong>
                        <textarea class="form-control">
                        </textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-10">
                        <button class="btn btn-default btn-lg pull-right">Contact Us</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



<section class="container-fluid" id="section7">
    <div class="row">
        <!--fontawesome icons-->
    
        <div class="col-sm-2 col-xs-4 text-center">
            <i class="fa fa-foursquare fa-4x"></i>
        </div>
        <div class="col-sm-2 col-xs-4 text-center">
            <i class="fa fa-pinterest fa-4x"></i>
        </div>
        <div class="col-sm-2 col-xs-4 text-center">
            <i class="fa fa-google-plus fa-4x"></i>
        </div>
        <div class="col-sm-2 col-xs-4 text-center">
            <i class="fa fa-twitter fa-4x"></i>
        </div>
         <div class="col-sm-2 col-xs-4 text-center">
            <i class="fa fa-facebook fa-4x"></i>
        </div>
           <div class="col-sm-2 col-xs-4 text-center">
            <i class="fa fa-digg fa-4x"></i>
        </div>
    
    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 column">
                <h4>Information</h4>
                <ul class="nav">
                	<li><a href="about-us.html">Specials</a></li>
                    <li><a href="about-us.html">Apply For Finance</a></li>
                    <li><a href="about-us.html">Book Test Drive</a></li>

                </ul>
            </div>
            <div class="col-xs-6 col-md-3 column">
                <h4>Follow Us</h4>
                <ul class="nav">
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Google+</a></li>
                    <li><a href="#">Pinterest</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-3 column">
                <h4>Showrooms</h4>
                <ul class="nav">
                    <li><a href="#">Pinetown</a></li>
                    <li><a href="#">Uhmlanga</a></li>
                    <li><a href="#">Central Durban</a></li>

                </ul>
            </div>
            <div class="col-xs-6 col-md-3 column">
                <h4>Customer Service</h4>
                <ul class="nav">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms &amp; Conditions</a></li>
                </ul>
            </div>
        </div>
        <!--/row-->
        <p class="text-right">©2015, KZN CARS - KZN Car Search Portal</p>
    </div>
</footer>


   <!-- Modal -->
  <div class="modal fade" id="infoModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="modal-title">Heading</h4>
        </div>
        <div class="modal-body" id="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


    <!--scripts loaded here-->
    
    
    <script src="/static/js/bootstrap.min.js"></script>
    
    <script src="/static/js/scripts.js"></script>
    <script src="/static/js/makes.json.js"></script>
    <script type="text/javascript">
    $(function(){
        $("#make-input").append(new Option("---", "-1"));
        $("#model-input").append(new Option("---", "-1"));
        for(i in makes_data){
          var make = makes_data[i];
          $("#make-input").append(new Option(make.name, make.id));
        }

        $("#make-input").change(function(){
             $("#model-input").children().remove()
             $("#model-input").append(new Option("---", "-1"));
            $.ajax({
                'url': '<?= BASE_URL;?>ajax.php?service=get_models&make_id=' + $(this).val()
            }).done(function(models) {
                models = JSON.parse(models);
                for(i in models){
                    m = models[i];
                    $("#model-input").append(new Option(m.name, m.id));
                }
            });
        });


    });
    function showModal(heading, description){
        $("#modal-title").html(heading);
        $("#modal-body").html(description);
        $("#infoModal");
         $("#infoModal").modal();
    }
   
    </script>
 
  

  </body>
</html>
