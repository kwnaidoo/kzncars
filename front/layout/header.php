<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>KZN CARS</title>
    <meta name="description" content="KZN Cars - a super saiyan car search" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="/static/js/jquery.min.js"></script>
    <link rel="stylesheet" href="/static/css/bootstrap.min.css" />
    <link href="/static/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/static/css/animate.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="/static/css/styles.css" />
  </head>
  <body >
    <nav class="navbar navbar-trans navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapsible">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand text-danger" href="#">KZN CARS</a>
        </div>
        <div class="navbar-collapse collapse" id="navbar-collapsible">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="#section1">Search Cars</a></li>

                <li><a href="#section3">About us</a></li>
                <li><a href="#section4">Contact us</a></li>
    
                <li>&nbsp;</li>
            </ul>
  
        </div>
    </div>
</nav>
