<?php
/** CONFIG **/

class DbConfig{
	public $host = "localhost";
	public $user = "kzncars";
	public $pass = "RwCskWgAth";
	public $db = "cars";
}
/** END CONFIG **/
/**
	A simple database abstraction layer for MYSQL, This ideally should be updated
	to use PDO instead.
**/

class Database{

//variable which will store the database connection
public $conn;

			//when the class  is instantiated - call the connect method to open the db connection
			function __construct(){
				$this->connect();
			}
			
			//connect to db
			function connect(){
				/**
				   instantiate our database configuration class to get the relevant credentials to 
				   open a db connection.
			    **/
				
				$dbconfig = new DbConfig();
				$host = $dbconfig->host;
				$user = $dbconfig->user;
				$password = $dbconfig->pass;
				$db  = $dbconfig->db;
				$conn = mysql_connect($host,$user,$password);
				
				//if the connection was successful - store the connection in $conn and select the db to work with. 
				
				if($conn){
						$this->conn = $conn;
						mysql_select_db($db);
				}else{
						$this->conn = false;
					}
					
			
			}
			
			//wrapper function for mysql_query
			function query($sql){
						if($this->conn){
								
								return mysql_query($sql);
								
						}else{
						
							return false;
						
						}
						
			}
			function last_id(){
				return mysql_insert_id();
			
			}
			//fetch one row and return the data as an object
			function fetch($result){
				return mysql_fetch_object($result);
			}
			//fetch one row and return the data as an array
			function fetch_a($result){
				return mysql_fetch_array($result);
			}
			
				//fetch many - return an array of objects
			function fetch_many($result){
				$rows = array();
				while($row = $this->fetch($result)){
					$rows[] = $row;
				}
				return $rows;
			}
			
			
			//fetch many - return an array of arrays
			function fetch_a_many($result){
				$rows = array();
				while($row = $this->fetch_a($result)){
					$rows[] = $row;
				}
				return $rows;
			}
			
		
			
			
			
			//return the number of rows affected by query
			function affected($result=null){
					
			     /**
				 if a result is present - that means that the query run was
				 a query that returned database rows of data therefore mysql_num_rows need 
				 to be used otherwise - its an update / Insert or delete so mysql_affected_rows
				 should be used instead
				 **/
					if($result!==null){
						return mysql_num_rows($result);
					}else{
						return mysql_affected_rows();
					}
			}
			
			
			/// Sanitize our data to prevent SQL and HTML injection attacks.
			function clean($value){
					if(is_array($value)){

						foreach($value as $key=>$val){
							$value[$key] = mysql_real_escape_string(htmlentities($val));

						}
					}else{
					$value = mysql_real_escape_string(htmlentities($value));

					}
			return $value;
			}

			
			//close db connection wrapper for mysql_close
			function close(){
				
				if($this->conn){ @mysql_close($this->conn);}
			}
			
			//close the connection if the object is about to be deleted from memory
			 function __destruct() {
				$this->close();
   }


}


