DROP PROCEDURE IF EXISTS get_car;
DELIMITER //
CREATE PROCEDURE get_car(IN car_id INT(11))
BEGIN
 SELECT makes.name as make, models.name as model , car_images.file as picture, cars.year, cars.id, 
        cars.price, cars.mileage from cars LEFT JOIN makes ON(makes.id=cars.make_id)
  LEFT JOIN models ON (models.id=cars.make_id) 
  LEFT JOIN car_images ON(car_images.car_id=cars.id) WHERE cars.id=car_id;

END //
DELIMITER ;

DROP PROCEDURE IF EXISTS get_cars;
DELIMITER //
CREATE PROCEDURE get_cars()
BEGIN
  SELECT makes.name as make, models.name as model , car_images.file as picture, cars.year, cars.id, 
        cars.price, cars.mileage from cars LEFT JOIN makes ON(makes.id=cars.make_id)
  LEFT JOIN models ON (models.id=cars.make_id) 
  LEFT JOIN car_images ON(car_images.car_id=cars.id);

END //
DELIMITER ;

DROP PROCEDURE IF EXISTS get_makes;
DELIMITER //
CREATE PROCEDURE get_makes()
BEGIN
 SELECT * from makes;

END //
DELIMITER ;

DROP PROCEDURE IF EXISTS get_models;
DELIMITER //
CREATE PROCEDURE get_model()
BEGIN
 SELECT * from models;

END //
DELIMITER ;


DROP PROCEDURE IF EXISTS get_models_by_make;
DELIMITER //
CREATE PROCEDURE get_models_by_make(IN make_id INT(11))
BEGIN
 SELECT * from models where make_id=make_id;

END //
DELIMITER ;


DROP PROCEDURE IF EXISTS search_cars;
DELIMITER //
CREATE PROCEDURE search_cars
(IN make_id INT(11), IN model_id INT(11), IN price_from DECIMAL(18,2), IN price_to DECIMAL(18,2))
BEGIN
    SET @fields = "SELECT makes.name as make, models.name as model , car_images.file as picture, cars.year, cars.id, 
        cars.price, cars.mileage, cars.fuel_type, 
        cars.aircon, cars.fuel_type, cars.transmission_type , 
        cars.abs, cars.doors, cars.type, cars.radio, cars.cd_player, cars.colour, cars.description";
    SET @joins = " from cars LEFT JOIN makes ON(makes.id=cars.make_id)
         LEFT JOIN models ON (models.id=cars.make_id) 
         LEFT JOIN car_images ON(car_images.car_id=cars.id)";

    IF make_id IS NOT NULL AND model_id IS NOT NULL THEN
         SET @where =CONCAT(" where cars.model_id=", model_id, " AND 
         cars.make_id=" , make_id , " and (price BETWEEN ", price_from , " and ", price_to, ")"); 
   
    ELSEIF make_id IS NOT NULL AND model_id IS NULL THEN
         SET @where =CONCAT(" where cars.make_id=", make_id ,
            " AND (price BETWEEN ", price_from," and ", price_to, ")");

    ELSEIF model_id IS NOT NULL AND make_id IS NULL THEN
        SET @where = CONCAT(" where cars.model_id=", model_id," AND (price BETWEEN " ,
            price_from ," and ", price_to, ")");
    ELSE
        SET @where =CONCAT(" where (price BETWEEN " , price_from, " and ", price_to, ")");
    END IF;

    SET @query = CONCAT(@fields, @joins, @where);
    PREPARE stmt FROM @query;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END //
DELIMITER ;