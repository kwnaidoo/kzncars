<section id="searchform" class="container-fluid">
<form class="form-inline" method="POST" id="car-search-form">
  <div class="form-group">
  <div class="form-group">
    &nbsp;&nbsp;
  &nbsp;&nbsp;
    &nbsp;&nbsp;
  &nbsp;&nbsp;    &nbsp;&nbsp;
  &nbsp;&nbsp;    &nbsp;&nbsp;
  &nbsp;&nbsp;
    <label for="Make">Make:</label>
    <select class="form-control" id="make-input" name="make">

    </select>
  </div>
    &nbsp;&nbsp;
    <label for="Model">Model:</label>
    <select class="form-control" id="model-input" name="model">

    </select>
  </div>

  &nbsp;&nbsp;
  <div class="form-group">
    <label for="From Price">Price(From):</label>
        <select class="form-control" id="price-from-input" name="price_from">
          <option value="1000" selected>R1000.00</option>
           <option value="5000">R5000.00</option>
           <option value="10000">R10000.00</option>
            <option value="20000">R20000.00</option>
            <option value="30000">R30000.00</option>
            <option value="50000">R50000.00</option>
    </select>
  </div>
  &nbsp;&nbsp;
  <div class="form-group">
    <label for="To Price">Price(To):</label>
       <select class="form-control" id="price-to-input" name="price_to">
        <option value="50000">R50000</option>
          <option value="50000">R75000</option>
        <option value="100000">R100000</option>
        <option value="150000">R150000</option>
        <option value="200000">R200000</option>
        <option value="250000">R250000</option>
        <option value="300000">R300000</option>
        <option value="500000">R500000</option>
        <option value="1000000" selected>R1000000</option>
   </select>
  </div>
  &nbsp;&nbsp;  &nbsp;&nbsp;
  &nbsp;&nbsp;
  &nbsp;&nbsp;
  &nbsp;&nbsp;
  &nbsp;&nbsp;

  <button type="submit" class="btn  btn-primary btn-md" id="searchbutton">Search</button>
</form>

</section>

<script type="text/javascript">
$(function(){
	$("#searchbutton").click(function(event){
		event.preventDefault();
    if(!$("#make-input").val().match("-1") && !$("#model-input").val().match("-1")){
		    $("#car-search-form").attr("action",$("#make-input option:selected").text() + "-" +  $("#model-input option:selected").text()+"-search-in-kwa-zulu-natal.html");
		}else if(!$("#make-input").val().match("-1")){
      $("#car-search-form").attr("action",$("#make-input option:selected").text() + "-search-in-kwa-zulu-natal.html");
    }else{
      $("#car-search-form").attr("action","search-all-cars-in-kwa-zulu-natal.html");
    }
    $("#car-search-form").submit();
	});
});
</script>
