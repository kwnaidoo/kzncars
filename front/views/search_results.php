<? global $cars;?>
<section class="container-fluid" id="search-banner-section">

    <a href="#cars-display-info">
    <div class="scroll-down bounceInDown animated">
            <span>
                <img src="/static/icons/more.png">
            </span>
    </div>
        </a>
</section>
<section style="background-color:#f6f6f6">
<div class="container" id="cars-display-info">
<?php require_once(VIEW_PATH."_search.php");?>
<? foreach($cars as $car):?>

<div class="panel panel-info">
      <div class="panel-heading"><?= $car->type == "new" ? "New " : "Pre-owned ";?><?= $car->year;?> <?= $car->make;?> <?= $car->model ;?></div>
      <div class="panel-body"><p>
       <div class="row">
            <div class="col-lg-3">
            <img src="<?= BASE_URL.$car->picture;?>" alt="img" style="max-width:150px;max-height:150px;" /><br />
            <button class="btn btn-primary" data-toggle="modal" data-target="#contactModal" style="min-width:150px;">Contact Dealer</button>
            </div>
               <div class="col-lg-9">
     <ul class="list-group">
    <li class="list-group-item list-group-item-success">
    <strong>Price: </strong>
     <?= CURRENCY_SYMBOL.$car->price.SPACER;?>

     <strong>Mileage: </strong>
      <?=$car->mileage.DISTANCE_UNIT.SPACER;?>

       <strong>Transmission: </strong>
      <?=$car->transmission_type.SPACER;?>
       <strong>Fuel Type: </strong>
      <?=$car->fuel_type.SPACER;?>
     </li>
    <li class="list-group-item list-group-item-info">
      <strong>Aircon: </strong>
      <?= $car->aircon == 1 ? "Yes" : "No";?><?= SPACER;?>
      <strong>ABS: </strong>
      <?= $car->abs == 1 ? "Yes" : "No";?><?= SPACER;?>
      <strong>Radio: </strong>
      <?= $car->radio == 1 ? "Yes" : "No";?><?= SPACER;?>
      <strong>CD Player: </strong>
      <?= $car->cd_player == 1 ? "Yes" : "No";?><?= SPACER;?>
       <strong>Doors: </strong>
      <?= $car->doors;?><?= SPACER;?>

      <?php if(isset($car->colour)):?>
        <strong>Colour: </strong>
      <?= $car->colour.SPACER;?>
      <?php endif;?>
    </li>
   

   
</ul>
<?php if(strlen($car->description) < 200):?>
<?= htmlentities($car->description);?>
<?php else:?>
  <?= htmlentities(substr($car->description, 0, 200));?>...
  <?php $description =escapeJavaScriptText(htmlentities($car->description));?>
  <button onclick="showModal('Description', '<?= $description;?>')" class="btn btn-primary btn-xs">read more</button>
  <?php endif;?>
               </div>
        </div>
        </p>
      </div>
</div>



<? endforeach; ?>
</div>
</section>


   <!-- Contact Dealer Modal -->
   <style>
   .modal-body .form-horizontal .col-sm-2,
   .modal-body .form-horizontal .col-sm-10 {
       width: 100%
   }

   .modal-body .form-horizontal .control-label {
       text-align: left;
   }
   .modal-body .form-horizontal .col-sm-offset-2 {
       margin-left: 15px;
   }
   </style>
 <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Contact Dealer
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form role="form">
                <div class="form-group">
                    <label for="fullNameInput">Full Name</label>
                      <input type="text" class="form-control"
                      id="fullNameInput" placeholder="Enter your full name"/>
                  </div>
                  <div class="form-group">
                    <label for="emailInput">Email address</label>
                      <input type="email" class="form-control"
                      id="emailInput" placeholder="Enter email"/>
                  </div>
                
                     <div class="form-group">
                    <label for="cellphoneInput">Cellphone Number</label>
                      <input type="text" class="form-control"
                      id="cellphoneInput" placeholder="Enter celphone number"/>
                  </div>
                     <div class="form-group">
                    <label for="messageTextArea">Message</label>
                      <textarea class="form-control" id="messageTextArea">
                      </textarea>
                  </div>
            
                  <button type="submit" class="btn btn-default">Submit</button>
                </form>
                
                
            </div>
            
    
         
        </div>
    </div>
</div>



