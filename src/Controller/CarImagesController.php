<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CarImages Controller
 *
 * @property \App\Model\Table\CarImagesTable $CarImages
 */
class CarImagesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index($id=null)
    {

        $this->__set_get_session($id);
        $this->set('carImages', $this->paginate($this->CarImages));
        $this->set('_serialize', ['carImages']);
    }


    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($id=null)
    {
        $this->__set_get_session($id);
        $carImage = $this->CarImages->newEntity();
        if ($this->request->is('post')) {
            $upload_info = upload_file($this->request->data['file']);
            if(isset($upload_info['path'])){
                $this->request->data['file'] = $upload_info['path'];
                $carImage = $this->CarImages->patchEntity($carImage, $this->request->data);
                if ($this->CarImages->save($carImage)) {
                    $this->Flash->success(__('The car image has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The car image could not be saved. Please, try again.'));
                }
            }else{
                $error = $upload_info['error'];
                $this->Flash->error(__("$error Please, try again."));
            }
        }
        $this->set(compact('carImage'));
        $this->set('_serialize', ['carImage']);
    }

   
    /**
     * Delete method
     *
     * @param string|null $id Car Image id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $carImage = $this->CarImages->get($id);
        if ($this->CarImages->delete($carImage)) {
            $file = $carImage->file;
            @unlink(WWW_ROOT.$file);
            $this->Flash->success(__('The car image has been deleted.'));
        } else {
            $this->Flash->error(__('The car image could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    private function __set_get_session($id){
        $session = $this->request->session();
        if($id!=null){
            $session->write("Car.id", (int)$id);
        }
        if(!$session->check("Car.id")){
           $this->Flash->error("Sorry, cannot find car ID - please click on 'Add Pic' or 'Show Pics' next to the car you wish to view/update pics.");
           $this->redirect(['controller' => 'cars', 'action' => 'index']);
        }
    }
}