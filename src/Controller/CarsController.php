<?php
namespace App\Controller;

use App\Controller\AppAuthController;
use Cake\Core\Configure;
/**
 * Cars Controller
 *
 * @property \App\Model\Table\CarsTable $Cars
 */
class CarsController extends AppAuthController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Makes', 'Models', 'Dealers', 'Suburbs']
        ];

        $this->set('cars', $this->paginate($this->Cars));
        $this->set('_serialize', ['cars']);
    }

    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $car = $this->Cars->newEntity();
        if ($this->request->is('post')) {

            $car = $this->Cars->patchEntity($car, $this->request->data);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('The car has been saved. Please add some images..'));
                return $this->redirect(['controller' => 'CarImages','action' => 'add', $car->id]);
            } else {
                $this->Flash->error(__('The car could not be saved. Please, try again.'));
            }
        }
        $makes = $this->Cars->Makes->find('list', ['limit' => 200]);
        $models = $this->Cars->Models->find('list', ['limit' => 200]);
        $dealers = $this->Cars->Dealers->find('list', ['limit' => 200]);
        $suburbs = $this->Cars->Suburbs->find('list', ['limit' => 200]);
        $body_types = Configure::read("BodyTypes");
        $this->set(compact('car', 'makes', 'models', 'dealers', 'suburbs','body_types'));
        $this->set('_serialize', ['car']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Car id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $car = $this->Cars->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $car = $this->Cars->patchEntity($car, $this->request->data);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('The car has been saved.'));
                 return $this->redirect(['controller' => 'CarImages','action' => 'index', $car->id]);
            } else {
                $this->Flash->error(__('The car could not be saved. Please, try again.'));
            }
        }
        $makes = $this->Cars->Makes->find('list', ['limit' => 200]);
        $models = $this->Cars->Models->find('list', ['limit' => 200]);
        $dealers = $this->Cars->Dealers->find('list', ['limit' => 200]);
        $suburbs = $this->Cars->Suburbs->find('list', ['limit' => 200]);
        $body_types = Configure::read("BodyTypes");
        $this->set(compact('car', 'makes', 'models', 'dealers', 'suburbs', 'body_types'));
        $this->set('_serialize', ['car']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Car id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $car = $this->Cars->get($id);
        if ($this->Cars->delete($car)) {
            $this->Flash->success(__('The car has been deleted.'));
        } else {
            $this->Flash->error(__('The car could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
