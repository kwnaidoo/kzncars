<?php
namespace App\Controller;

use App\Controller\AppAuthController;
use Cake\Core\Configure;
/**
 * CustomerDocuments Controller
 *
 * @property \App\Model\Table\CustomerDocumentsTable $CustomerDocuments
 */
class CustomerDocumentsController extends AppAuthController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Customers']
        ];
        $this->set('customerDocuments', $this->paginate($this->CustomerDocuments));
        $this->set('_serialize', ['customerDocuments']);
    }

    /**
     * View method
     *
     * @param string|null $id Customer Document id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $customerDocument = $this->CustomerDocuments->get($id, [
            'contain' => ['Customers']
        ]);
        $this->set('customerDocument', $customerDocument);
        $this->set('_serialize', ['customerDocument']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */


        public function add()
    {
        $customerDocument = $this->CustomerDocuments->newEntity();
        if ($this->request->is('post')) {
            $upload_info = upload_file($this->request->data['document'], 
                Configure::read('doc_formats'), "docs/");
            if(isset($upload_info['path'])){
                $this->request->data['document'] = $upload_info['path'];
                $customerDocument = $this->CustomerDocuments->patchEntity($customerDocument, $this->request->data);
                if ($this->CustomerDocuments->save($customerDocument)) {
                    $this->Flash->success(__('The document has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The document could not be saved. Please, try again.'));
                }
            }else{
                $error = $upload_info['error'];
                $this->Flash->error(__("$error Please, try again."));
            }
        }
        $customers = $this->CustomerDocuments->Customers->find('list', ['limit' => 200]);
        $this->set(compact('customerDocument', 'customers'));
        $this->set('_serialize', ['customerDocument']);
    }



}
