<?php
namespace App\Controller;

use App\Controller\AppAuthController;
use Cake\Event\Event;
use Cake\Core\Configure;
/**
 * CustomerTasks Controller
 *
 * @property \App\Model\Table\CustomerTasksTable $CustomerTasks
 */
class CustomerTasksController extends AppAuthController
{

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $statuses = Configure::read("statuses");
        $urgency_levels = Configure::read("urgency_levels");
        $this->set(compact('statuses', 'urgency_levels'));
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Customers']
        ];
        $this->set('customerTasks', $this->paginate($this->CustomerTasks));
        $this->set('_serialize', ['customerTasks']);
    }

    /**
     * View method
     *
     * @param string|null $id Customer Task id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $customerTask = $this->CustomerTasks->get($id, [
            'contain' => ['Customers']
        ]);
        $this->set('customerTask', $customerTask);
        $this->set('_serialize', ['customerTask']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $customerTask = $this->CustomerTasks->newEntity();
        if ($this->request->is('post')) {
            $customerTask = $this->CustomerTasks->patchEntity($customerTask, $this->request->data);
            if ($this->CustomerTasks->save($customerTask)) {
                $this->Flash->success(__('The customer task has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The customer task could not be saved. Please, try again.'));
            }
        }
        $customers = $this->CustomerTasks->Customers->find('list', ['limit' => 200]);
        $this->set(compact('customerTask', 'customers'));
        $this->set('_serialize', ['customerTask']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Customer Task id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customerTask = $this->CustomerTasks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customerTask = $this->CustomerTasks->patchEntity($customerTask, $this->request->data);
            if ($this->CustomerTasks->save($customerTask)) {
                $this->Flash->success(__('The customer task has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The customer task could not be saved. Please, try again.'));
            }
        }
        $customers = $this->CustomerTasks->Customers->find('list', ['limit' => 200]);
        $this->set(compact('customerTask', 'customers'));
        $this->set('_serialize', ['customerTask']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Customer Task id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customerTask = $this->CustomerTasks->get($id);
        if ($this->CustomerTasks->delete($customerTask)) {
            $this->Flash->success(__('The customer task has been deleted.'));
        } else {
            $this->Flash->error(__('The customer task could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
