<?php
namespace App\Controller;

use App\Controller\AppAuthController;

/**
 * Dealers Controller
 *
 * @property \App\Model\Table\DealersTable $Dealers
 */
class DealersController extends AppAuthController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('dealers', $this->paginate($this->Dealers));
        $this->set('_serialize', ['dealers']);
    }

    /**
     * View method
     *
     * @param string|null $id Dealer id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dealer = $this->Dealers->get($id, [
            'contain' => ['Users', 'Cars']
        ]);
        $this->set('dealer', $dealer);
        $this->set('_serialize', ['dealer']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dealer = $this->Dealers->newEntity();
        if ($this->request->is('post')) {
            $dealer = $this->Dealers->patchEntity($dealer, $this->request->data);
            if ($this->Dealers->save($dealer)) {
                $this->Flash->success(__('The dealer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dealer could not be saved. Please, try again.'));
            }
        }
        $users = $this->Dealers->Users->find('list', ['limit' => 200]);
        $this->set(compact('dealer', 'users'));
        $this->set('_serialize', ['dealer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dealer id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dealer = $this->Dealers->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dealer = $this->Dealers->patchEntity($dealer, $this->request->data);
            if ($this->Dealers->save($dealer)) {
                $this->Flash->success(__('The dealer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dealer could not be saved. Please, try again.'));
            }
        }
        $users = $this->Dealers->Users->find('list', ['limit' => 200]);
        $this->set(compact('dealer', 'users'));
        $this->set('_serialize', ['dealer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dealer id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dealer = $this->Dealers->get($id);
        if ($this->Dealers->delete($dealer)) {
            $this->Flash->success(__('The dealer has been deleted.'));
        } else {
            $this->Flash->error(__('The dealer could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
