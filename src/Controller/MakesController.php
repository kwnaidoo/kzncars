<?php
namespace App\Controller;

use App\Controller\AppAuthController;

/**
 * Makes Controller
 *
 * @property \App\Model\Table\MakesTable $Makes
 */
class MakesController extends AppAuthController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('makes', $this->paginate($this->Makes));
        $this->set('_serialize', ['makes']);
    }

    /**
     * View method
     *
     * @param string|null $id Make id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $make = $this->Makes->get($id, [
            'contain' => ['Cars']
        ]);
        $this->set('make', $make);
        $this->set('_serialize', ['make']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $make = $this->Makes->newEntity();
        if ($this->request->is('post')) {
            $make = $this->Makes->patchEntity($make, $this->request->data);
            if ($this->Makes->save($make)) {
                $this->Flash->success(__('The make has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The make could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('make'));
        $this->set('_serialize', ['make']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Make id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $make = $this->Makes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $make = $this->Makes->patchEntity($make, $this->request->data);
            if ($this->Makes->save($make)) {
                $this->Flash->success(__('The make has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The make could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('make'));
        $this->set('_serialize', ['make']);
    }


}
