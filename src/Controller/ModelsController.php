<?php
namespace App\Controller;

use App\Controller\AppAuthController;

/**
 * Models Controller
 *
 * @property \App\Model\Table\ModelsTable $Models
 */
class ModelsController extends AppAuthController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Makes']
        ];
        $this->set('models', $this->paginate($this->Models));
        $this->set('_serialize', ['models']);
    }

    /**
     * View method
     *
     * @param string|null $id Model id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $model = $this->Models->get($id, [
            'contain' => ['Makes', 'Cars']
        ]);
        $this->set('model', $model);
        $this->set('_serialize', ['model']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $model = $this->Models->newEntity();
        if ($this->request->is('post')) {
            $model = $this->Models->patchEntity($model, $this->request->data);
            if ($this->Models->save($model)) {
                $this->Flash->success(__('The model has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The model could not be saved. Please, try again.'));
            }
        }
        $makes = $this->Models->Makes->find('list', ['limit' => 200]);
        $this->set(compact('model', 'makes'));
        $this->set('_serialize', ['model']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Model id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $model = $this->Models->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $model = $this->Models->patchEntity($model, $this->request->data);
            if ($this->Models->save($model)) {
                $this->Flash->success(__('The model has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The model could not be saved. Please, try again.'));
            }
        }
        $makes = $this->Models->Makes->find('list', ['limit' => 200]);
        $this->set(compact('model', 'makes'));
        $this->set('_serialize', ['model']);
    }

 

    public function get_models($id){
        $models = $this->Models->find('list', ['limit' => 200])->where(['make_id'=>$id]);
        $data = array();
        foreach($models as $key=>$value){
            $data[] = array($key, $value);
        }
        print json_encode($data);
        die;
    }
}
