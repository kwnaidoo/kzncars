<?php
namespace App\Controller;

use App\Controller\AppAuthController;

/**
 * Showrooms Controller
 *
 * @property \App\Model\Table\ShowroomsTable $Showrooms
 */
class ShowroomsController extends AppAuthController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dealers'],
            'finder' => 'ByDealer',
            'session' => $this->request->session()
        ];
        $this->set('showrooms', $this->paginate($this->Showrooms));
        $this->set('_serialize', ['showrooms']);
    }

    /**
     * View method
     *
     * @param string|null $id Showroom id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $showroom = $this->Showrooms->get($id, [
            'contain' => ['Dealers']
        ]);
        $this->set('showroom', $showroom);
        $this->set('_serialize', ['showroom']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $showroom = $this->Showrooms->newEntity();
        if ($this->request->is('post')) {
            $showroom = $this->Showrooms->patchEntity($showroom, $this->request->data);
            if ($this->Showrooms->save($showroom)) {
                $this->Flash->success(__('The showroom has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The showroom could not be saved. Please, try again.'));
            }
        }
        if($this->request->session()->read("User.user_type") == "super"){
        $dealers = $this->Showrooms->Dealers->find('list', ['limit' => 200]);
        }else{
            $dealers = $this->Showrooms->Dealers->find('list',
             ['where'=>['id'=>$this->request->session()->read("User.dealer_id")]]);
        }
        $this->set(compact('showroom', 'dealers'));
        $this->set('_serialize', ['showroom']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Showroom id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $showroom = $this->Showrooms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $showroom = $this->Showrooms->patchEntity($showroom, $this->request->data);
            if ($this->Showrooms->save($showroom)) {
                $this->Flash->success(__('The showroom has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The showroom could not be saved. Please, try again.'));
            }
        }
        $dealers = $this->Showrooms->Dealers->find('list', ['limit' => 200]);
        $this->set(compact('showroom', 'dealers'));
        $this->set('_serialize', ['showroom']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Showroom id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $showroom = $this->Showrooms->get($id);
        if ($this->Showrooms->delete($showroom)) {
            $this->Flash->success(__('The showroom has been deleted.'));
        } else {
            $this->Flash->error(__('The showroom could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
