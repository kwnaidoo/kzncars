<?php
namespace App\Controller;

use App\Controller\AppAuthController;

/**
 * Suburbs Controller
 *
 * @property \App\Model\Table\SuburbsTable $Suburbs
 */
class SuburbsController extends AppAuthController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('suburbs', $this->paginate($this->Suburbs));
        $this->set('_serialize', ['suburbs']);
    }

    /**
     * View method
     *
     * @param string|null $id Suburb id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $suburb = $this->Suburbs->get($id, [
            'contain' => ['Cars']
        ]);
        $this->set('suburb', $suburb);
        $this->set('_serialize', ['suburb']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $suburb = $this->Suburbs->newEntity();
        if ($this->request->is('post')) {
            $suburb = $this->Suburbs->patchEntity($suburb, $this->request->data);
            if ($this->Suburbs->save($suburb)) {
                $this->Flash->success(__('The suburb has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The suburb could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('suburb'));
        $this->set('_serialize', ['suburb']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Suburb id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $suburb = $this->Suburbs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $suburb = $this->Suburbs->patchEntity($suburb, $this->request->data);
            if ($this->Suburbs->save($suburb)) {
                $this->Flash->success(__('The suburb has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The suburb could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('suburb'));
        $this->set('_serialize', ['suburb']);
    }

 
}
