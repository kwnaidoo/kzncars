<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $session = $this->request->session();
        if(!$session->check("User")){
            $this->redirect("/users/login");
        }
         
        $this->paginate = [
            'contain' => ['Dealers']
        ];
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function login(){
        $user = $this->Users->newEntity();
        if($this->request->is('post')){
            $user_data = $this->request->data;
            $user_data = $this->Users->find()->where(
                [
                "username"=>$user_data['username'],
                "password"=>$user_data['password'],
                ]
            )->first();
      
            if($user_data){
                $session = $this->request->session();
                $session->write("User.user_type", $user_data->user_type);
                $session->write("User.username", $user_data->username);
                $session->write("User.user_id", $user_data->id);
                $session->write("User.dealer_id", $user_data->dealer_id);
                $this->redirect("/cars/");
            }else {
                $this->Flash->error(__('Invalid username or password. Please, try again.'));
            }

        }
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function logout(){
        $this->request->session()->delete("User");
        $this->redirect("/users/login");
    }
    public function view($id = null)
    {
           $session = $this->request->session();
        if(!$session->check("User")){
            $this->redirect("/users/login");
        }
    
        $user = $this->Users->get($id, [
            'contain' => ['Dealers']
        ]);
        $this->set('user', $user);

        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
           $session = $this->request->session();
        if(!$session->check("User")){
            $this->redirect("/users/login");
        }
    
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $dealers = $this->Users->Dealers->find('list', ['limit' => 200]);
        $this->set(compact('user', 'dealers'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
           $session = $this->request->session();
        if(!$session->check("User")){
            $this->redirect("/users/login");
        }
    
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $dealers = $this->Users->Dealers->find('list', ['limit' => 200]);
        $this->set(compact('user', 'dealers'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
           $session = $this->request->session();
        if(!$session->check("User")){
            $this->redirect("/users/login");
        }
    
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
