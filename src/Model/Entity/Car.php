<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Car Entity.
 */
class Car extends Entity
{


    /**'pic_2',
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    
    protected $_accessible = [
        'price' => true,
        'mileage' => true,
        'fuel_type' => true,
        'transmission_type' => true,
        'make_id' => true,
        'model_id' => true,
        'dealer_id' => true,
        'main_image' => true,
        'pic2' => true,
        'pic3' => true,
        'pic5' => true,
        'pic4' => true,
        'year' => true,
        'suburb_id' => true,
        'radio' => true,
        'cd_player' => true,
        'abs' => true,
        'aircon' => true,
        'body_type' => true,
        'colour' => true,
        'doors' => true,
        'type' => true,
        'make' => true,
        'model' => true,
        'dealer' => true,
        'suburb' => true,
    ];
}
