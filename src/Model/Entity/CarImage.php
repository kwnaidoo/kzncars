<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CarImage Entity.
 */
class CarImage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'caption' => true,
        'file' => true,
        'car_id' => true,
    ];
}
