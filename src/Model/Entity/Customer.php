<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Customer Entity.
 */
class Customer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'surname' => true,
        'telephone' => true,
        'fax' => true,
        'cellphone' => true,
        'email' => true,
        'physical_address' => true,
        'postal_address' => true,
        'notes' => true,
        'customer_tasks' => true,
    ];
}
