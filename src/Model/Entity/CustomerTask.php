<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CustomerTask Entity.
 */
class CustomerTask extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'customer_id' => true,
        'title' => true,
        'status' => true,
        'urgency' => true,
        'details' => true,
        'customer' => true,
    ];
}
