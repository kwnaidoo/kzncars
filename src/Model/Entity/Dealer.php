<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dealer Entity.
 */
class Dealer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'company' => true,
        'company_profile' => true,
        'fax_number' => true,
        'telephone_number' => true,
        'operating_times' => true,
        'email_address' => true,
        'physical_address' => true,
        'postal_address' => true,
        'cars' => true,
        'users' => true,
    ];
}
