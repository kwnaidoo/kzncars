<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Lead Entity.
 */
class Lead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'car_id' => true,
        'first_name' => true,
        'surname' => true,
        'cellphone' => true,
        'telephone' => true,
        'message' => true,
        'user_id' => true,
        'car' => true,
    ];
}
