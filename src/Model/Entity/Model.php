<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Model Entity.
 */
class Model extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'make_id' => true,
        'make' => true,
        'cars' => true,
    ];
}
