<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity.
 */
class Page extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'page_id' => true,
        'content' => true,
        'meta_keywords' => true,
        'meta_description' => true,
        'rank' => true,
        'show_on_website' => true,
        'pages' => true,
    ];
}
