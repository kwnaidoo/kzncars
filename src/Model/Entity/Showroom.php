<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Showroom Entity.
 */
class Showroom extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'address' => true,
        'telephone_number' => true,
        'fax_number' => true,
        'email_address' => true,
        'profile' => true,
        'dealer_id' => true,
        'dealer' => true,
    ];
}
