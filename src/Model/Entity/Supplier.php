<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supplier Entity.
 */
class Supplier extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'company' => true,
        'telephone' => true,
        'fax' => true,
        'contact_person' => true,
        'email' => true,
        'physical_address' => true,
        'postal_address' => true,
        'notes' => true,
        'vat_number' => true,
        'reg_number' => true,
    ];
}
