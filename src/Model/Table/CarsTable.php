<?php
namespace App\Model\Table;

use App\Model\Entity\Car;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cars Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Makes
 * @property \Cake\ORM\Association\BelongsTo $Models
 * @property \Cake\ORM\Association\BelongsTo $Dealers
 * @property \Cake\ORM\Association\BelongsTo $Suburbs
 */
class CarsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('cars');

        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsTo('Makes', [
            'foreignKey' => 'make_id'
        ]);
        $this->belongsTo('Models', [
            'foreignKey' => 'model_id'
        ]);
        $this->belongsTo('Dealers', [
            'foreignKey' => 'dealer_id'
        ]);
        $this->belongsTo('Suburbs', [
            'foreignKey' => 'suburb_id'
        ]);
   
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('price', 'valid', ['rule' => 'decimal'])
            ->notEmpty('price');
            
        $validator
            ->notEmpty('mileage');
         $validator
            ->notEmpty('dealer_id');
        $validator
           ->notEmpty('suburb_id');
        $validator
           ->notEmpty('make_id');
        $validator
           ->notEmpty('model_id');
        $validator
            ->allowEmpty('fuel_type');
            
        $validator
            ->allowEmpty('transmission_type');
            
        $validator
            ->allowEmpty('main_image');
            
        $validator
            ->allowEmpty('pic2');
            
        $validator
            ->allowEmpty('pic3');
            
        $validator
            ->allowEmpty('pic5');
            
        $validator
            ->allowEmpty('pic4');
            
        $validator
            ->add('year', 'valid', ['rule' => 'numeric'])
            ->notEmpty('year');
            
        $validator
            ->add('radio', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('radio');
            
        $validator
            ->add('cd_player', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('cd_player');
            
        $validator
            ->add('abs', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('abs');
            
        $validator
            ->add('aircon', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('aircon');
            
        $validator
            ->allowEmpty('body_type');
            
        $validator
            ->allowEmpty('colour');
            
        $validator
            ->add('doors', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('doors');
            
        $validator
            ->allowEmpty('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['make_id'], 'Makes'));
        $rules->add($rules->existsIn(['model_id'], 'Models'));
        $rules->add($rules->existsIn(['dealer_id'], 'Dealers'));
        $rules->add($rules->existsIn(['suburb_id'], 'Suburbs'));
        return $rules;
    }
}
