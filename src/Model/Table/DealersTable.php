<?php
namespace App\Model\Table;

use App\Model\Entity\Dealer;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dealers Model
 *
 * @property \Cake\ORM\Association\HasMany $Cars
 * @property \Cake\ORM\Association\HasMany $Showrooms
 * @property \Cake\ORM\Association\HasMany $Users
 */
class DealersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('dealers');
        $this->displayField('company');
        $this->primaryKey('id');
        $this->hasMany('Cars', [
            'foreignKey' => 'dealer_id'
        ]);
        $this->hasMany('Showrooms', [
            'foreignKey' => 'dealer_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'dealer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->notEmpty('company');
            
        $validator
            ->allowEmpty('company_profile');
            
        $validator
            ->allowEmpty('fax_number');
            
        $validator
            ->notEmpty('telephone_number');
            
        $validator
            ->notEmpty('operating_times');
            
        $validator
            ->notEmpty('email_address')
            ->add('email_address', 'valid', 
                ['rule' => 'email' , 'message'=>"Email address invalid"]);
            
        $validator
            ->notEmpty('physical_address');
            
        $validator
            ->allowEmpty('postal_address');

        return $validator;
    }
}
