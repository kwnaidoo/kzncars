<?php
namespace App\Model\Table;

use App\Model\Entity\Make;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Makes Model
 *
 * @property \Cake\ORM\Association\HasMany $Cars
 * @property \Cake\ORM\Association\HasMany $Models
 */
class MakesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('makes');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->hasMany('Cars', [
            'foreignKey' => 'make_id'
        ]);
        $this->hasMany('Models', [
            'foreignKey' => 'make_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->notEmpty('name');

        return $validator;
    }
}
