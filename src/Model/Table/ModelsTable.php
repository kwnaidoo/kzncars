<?php
namespace App\Model\Table;

use App\Model\Entity\Model;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Models Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Makes
 * @property \Cake\ORM\Association\HasMany $Cars
 */
class ModelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('models');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->belongsTo('Makes', [
            'foreignKey' => 'make_id'
        ]);
        $this->hasMany('Cars', [
            'foreignKey' => 'model_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->notEmpty('id', 'create');
            
        $validator
            ->notEmpty('name');
        $validator
            ->notEmpty('make_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['make_id'], 'Makes'));
        return $rules;
    }
}
