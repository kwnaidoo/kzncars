<?php
namespace App\Model\Table;

use App\Model\Entity\Page;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Pages
 * @property \Cake\ORM\Association\HasMany $Pages
 */
class PagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('pages');
        $this->displayField('title');
        $this->primaryKey('id');
        $this->belongsTo('Pages', [
            'foreignKey' => 'page_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Pages', [
            'foreignKey' => 'page_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');
            
        $validator
            ->requirePresence('content', 'create')
            ->notEmpty('content');
            
        $validator
            ->requirePresence('meta_keywords', 'create')
            ->notEmpty('meta_keywords');
            
        $validator
            ->requirePresence('meta_description', 'create')
            ->notEmpty('meta_description');
            
        $validator
            ->requirePresence('rank', 'create')
            ->notEmpty('rank');
            
        $validator
            ->requirePresence('show_on_website', 'create')
            ->notEmpty('show_on_website');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['page_id'], 'Pages'));
        return $rules;
    }
}
