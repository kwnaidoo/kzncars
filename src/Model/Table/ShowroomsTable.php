<?php
namespace App\Model\Table;

use App\Model\Entity\Showroom;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Showrooms Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Dealers
 */
class ShowroomsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('showrooms');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->belongsTo('Dealers', [
            'foreignKey' => 'dealer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->notEmpty('name');
            
        $validator
            ->notEmpty('address');
            
        $validator
            ->notEmpty('telephone_number');
            
        $validator
            ->allowEmpty('fax_number');
            
        $validator
            ->notEmpty('email_address')
            ->add('email_address', 'valid', 
                ['rule' => 'email' , 'message'=>"Email address invalid"]);
            
        $validator
            ->allowEmpty('profile');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dealer_id'], 'Dealers'));
        return $rules;
    }
      public function findByDealer(Query $query, array $options)
    {
        $session = $options['session'];
        $user_type = $session->read("User.user_type");
        $dealer_id = $session->read("User.dealer_id");
        if($user_type!="super"){
        $query->where([
            'dealer_id' => $dealer_id
        ]);
    }
        return $query;
    }


}
