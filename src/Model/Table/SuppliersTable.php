<?php
namespace App\Model\Table;

use App\Model\Entity\Supplier;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Suppliers Model
 *
 */
class SuppliersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('suppliers');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('company', 'create')
            ->notEmpty('company');
            
        $validator
            ->requirePresence('telephone', 'create')
            ->notEmpty('telephone');
            
        $validator
            ->requirePresence('fax', 'create')
            ->notEmpty('fax');
            
        $validator
            ->requirePresence('contact_person', 'create')
            ->notEmpty('contact_person');
            
        $validator
            ->add('email', 'valid', ['rule' => 'email'])
            ->requirePresence('email', 'create')
            ->notEmpty('email');
            
        $validator
            ->requirePresence('physical_address', 'create')
            ->notEmpty('physical_address');
            
        $validator
            ->requirePresence('postal_address', 'create')
            ->notEmpty('postal_address');
            
        $validator
            ->requirePresence('notes', 'create')
            ->notEmpty('notes');
            
        $validator
            ->requirePresence('vat_number', 'create')
            ->notEmpty('vat_number');
            
        $validator
            ->requirePresence('reg_number', 'create')
            ->notEmpty('reg_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
}
