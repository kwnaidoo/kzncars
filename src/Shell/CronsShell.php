<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Psy\Shell as PsyShell;

/**
 * Simple console wrapper around Psy\Shell.
 */
class CronsShell extends Shell
{

    /**
     * Start the shell and interactive console.
     *
     * @return int|void
     */
    public function main()
    {
       $this->gen_makes_json();
    }

    public function gen_makes_json(){
        $this->loadModel('Makes');
        $makes = $this->Makes->find('all')->all();
        file_put_contents(WWW_ROOT."static/js/makes.json.js", "var makes_data =".json_encode($makes));

    }

}
