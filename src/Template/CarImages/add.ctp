
<div class="carImages form large-12 medium-12 columns">
    <?= $this->Form->create($carImage, ['type'=>'file']) ?>
    <fieldset>
        <legend><?= __('Add Car Image') ?></legend>
        <?php
            echo $this->Form->input('caption');
            echo $this->Form->input('file', ['type' => 'file']);
            $s = $this->request->Session();
            echo $this->Form->input('car_id', ['type'=> 'hidden', 'value'=> $s->read("Car.id") ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
