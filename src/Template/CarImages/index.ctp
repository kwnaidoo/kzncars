
<div class="carImages index large-12 medium-12 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('caption', 'Picture') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($carImages as $carImage): ?>
        <tr>
         
            <td><?= h($carImage->caption) ?><br />
            <img src="<?php print $this->request->webroot.$carImage->file;?>" style="max-width:150px;max-height:150px;"/>
            </td>

            <td class="actions">
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $carImage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $carImage->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
