
<div class="cars form large-12 medium-12 columns">
    <?= $this->Form->create($car) ?>
    <fieldset>
        <legend><?= __('Edit Car') ?></legend>
 
     
<div id="tabs">
    <ul>
        <li><a href="#general-info" active>General Information</a></li>
        <li><a href="#features">Features</a></li>
        <li><a href="#extras">Extras</a></li>
    
    </ul>
    <div id="general-info">
    <p>
           <?php
            //selects
            $booleans = array(0=>"No", 1=>"Yes");

                echo $this->Form->input('price');
                echo $this->Form->input('mileage');
                echo $this->Form->input('make_id', ['options' => $makes, 'empty' => true]);
                echo $this->Form->input('model_id', ['options' => $models, 'empty' => true]);
                echo $this->Form->input('dealer_id', ['options' => $dealers, 'empty' => true]);

                    echo $this->Form->input('year');
                echo $this->Form->input('suburb_id', ['options' => $suburbs, 'empty' => true]);
                    echo $this->Form->input('type', ['options' => ["new" => "New", "used"=>"Used"]]);
            ?>
    </p>
    </div>

    <div id="features">
    <p>
    <div>
    <?php

        echo $this->Form->input('body_type', ['options' => $body_types]);
        echo $this->Form->input('colour');
       echo $this->Form->input('doors', ['options'=> [4 => 4, 2 => 2, 5 => 5]]);

    ?>
    </div>
    </p>
    </div>

    <div id="extras">
    <p>
            <?php 

                echo $this->Form->input('radio', ["options"=>$booleans]);
                echo $this->Form->input('cd_player', ["options"=>$booleans]);
                echo $this->Form->input('abs' , ["options"=>$booleans]);
                echo $this->Form->input('aircon', ["options"=>$booleans]);

            ?>
    </p>
    </div>


</div>
  
    
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script>

$(function(){
    $("#model-id").empty();
    $base_url = '<?php print $this->Url->build(array("controller"=>"models","action"=>"get_models"));?>'
    $("#make-id").change(function(){

        $.ajax({url: $base_url + "/" + $(this).val()}).done(
            function(data) { 
                data = JSON.parse(data);
                 $("#model-id").empty();
                for(i in data){
                    var id = data[i][0];
                    var name = data[i][1];
                    var opt = new Option(name, id);
                    $("#model-id").append(opt);
                }
        });
    })
})
</script>