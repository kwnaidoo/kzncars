
<div class="cars index large-12 medium-12 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('price') ?></th>
            <th><?= $this->Paginator->sort('mileage') ?></th>

            <th><?= $this->Paginator->sort('make_id') ?></th>
            <th><?= $this->Paginator->sort('model_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($cars as $car): ?>
        <tr>
            <td><?= $this->Number->format($car->id) ?></td>
            <td><?= $this->Number->format($car->price) ?></td>
            <td><?= h($car->mileage) ?></td>
  
            <td>
                <?= $car->has('make') ? $this->Html->link($car->make->name, ['controller' => 'Makes', 'action' => 'view', $car->make->id]) : '' ?>
            </td>
            <td>
                <?= $car->has('model') ? $this->Html->link($car->model->name, ['controller' => 'Models', 'action' => 'view', $car->model->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('Add Pic'), ['controller' => 'CarImages', 'action' => 'add', $car->id]) ?><br />
                <?= $this->Html->link(__("Show Pic's"), ['controller' => 'CarImages', 'action' => 'index', $car->id]) ?><br />
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $car->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $car->id], ['confirm' => __('Are you sure you want to delete # {0}?', $car->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
