
<div class="cars view large-12 medium-12 columns">
    <h2><?= h($car->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Mileage') ?></h6>
            <p><?= h($car->mileage) ?></p>
            <h6 class="subheader"><?= __('Fuel Type') ?></h6>
            <p><?= h($car->fuel_type) ?></p>
            <h6 class="subheader"><?= __('Transmission Type') ?></h6>
            <p><?= h($car->transmission_type) ?></p>
            <h6 class="subheader"><?= __('Make') ?></h6>
            <p><?= $car->has('make') ? $this->Html->link($car->make->name, ['controller' => 'Makes', 'action' => 'view', $car->make->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Model') ?></h6>
            <p><?= $car->has('model') ? $this->Html->link($car->model->name, ['controller' => 'Models', 'action' => 'view', $car->model->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Dealer') ?></h6>
            <p><?= $car->has('dealer') ? $this->Html->link($car->dealer->id, ['controller' => 'Dealers', 'action' => 'view', $car->dealer->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Main Image') ?></h6>
            <p><?= h($car->main_image) ?></p>
            <h6 class="subheader"><?= __('Pic2') ?></h6>
            <p><?= h($car->pic2) ?></p>
            <h6 class="subheader"><?= __('Pic3') ?></h6>
            <p><?= h($car->pic3) ?></p>
            <h6 class="subheader"><?= __('Pic5') ?></h6>
            <p><?= h($car->pic5) ?></p>
            <h6 class="subheader"><?= __('Pic4') ?></h6>
            <p><?= h($car->pic4) ?></p>
            <h6 class="subheader"><?= __('Suburb') ?></h6>
            <p><?= $car->has('suburb') ? $this->Html->link($car->suburb->name, ['controller' => 'Suburbs', 'action' => 'view', $car->suburb->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Body Type') ?></h6>
            <p><?= h($car->body_type) ?></p>
            <h6 class="subheader"><?= __('Colour') ?></h6>
            <p><?= h($car->colour) ?></p>
            <h6 class="subheader"><?= __('Type') ?></h6>
            <p><?= h($car->type) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($car->id) ?></p>
            <h6 class="subheader"><?= __('Price') ?></h6>
            <p><?= $this->Number->format($car->price) ?></p>
            <h6 class="subheader"><?= __('Year') ?></h6>
            <p><?= $this->Number->format($car->year) ?></p>
            <h6 class="subheader"><?= __('Radio') ?></h6>
            <p><?= $this->Number->format($car->radio) ?></p>
            <h6 class="subheader"><?= __('Cd Player') ?></h6>
            <p><?= $this->Number->format($car->cd_player) ?></p>
            <h6 class="subheader"><?= __('Abs') ?></h6>
            <p><?= $this->Number->format($car->abs) ?></p>
            <h6 class="subheader"><?= __('Aircon') ?></h6>
            <p><?= $this->Number->format($car->aircon) ?></p>
            <h6 class="subheader"><?= __('Doors') ?></h6>
            <p><?= $this->Number->format($car->doors) ?></p>
        </div>
    </div>
</div>
