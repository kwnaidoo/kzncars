
<div class="customerDocuments form large-12 medium-12 columns">
    <?= $this->Form->create($customerDocument, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Add Customer Document') ?></legend>
        <?php
            echo $this->Form->input('customer_id', ['options' => $customers]);
            echo $this->Form->input('document', ['type' => 'file']);
            echo $this->Form->input('notes');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
