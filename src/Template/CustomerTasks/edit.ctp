
<div class="customerTasks form large-12 medium-12 columns">
    <?= $this->Form->create($customerTask) ?>
    <fieldset>
        <legend><?= __('Edit Customer Task') ?></legend>
        <?php
            echo $this->Form->input('customer_id', ['options' => $customers]);
            echo $this->Form->input('title');
             echo $this->Form->input('status', ['options' => $statuses]);
            echo $this->Form->input('urgency', ['options' => $urgency_levels]);
            echo $this->Form->input('details');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
