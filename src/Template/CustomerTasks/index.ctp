
<div class="customerTasks index large-12 medium-12 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('customer_id') ?></th>
            <th><?= $this->Paginator->sort('title') ?></th>
            <th><?= $this->Paginator->sort('status') ?></th>
            <th><?= $this->Paginator->sort('urgency') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($customerTasks as $customerTask): ?>
        <tr>
            <td><?= $this->Number->format($customerTask->id) ?></td>
            <td>
                <?= $customerTask->has('customer') ? $this->Html->link($customerTask->customer->id, ['controller' => 'Customers', 'action' => 'view', $customerTask->customer->id]) : '' ?>
            </td>
            <td><?= h($customerTask->title) ?></td>
            <td><?= h($customerTask->status) ?></td>
            <td><?= h($customerTask->urgency) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $customerTask->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customerTask->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customerTask->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customerTask->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
