
<div class="customers form large-12 medium-12 columns">
    <?= $this->Form->create($customer) ?>
    <fieldset>
        <legend><?= __('Add Customer') ?></legend>
        <?php
            echo $this->Form->input('first_name');
            echo $this->Form->input('surname');
            echo $this->Form->input('telephone');
            echo $this->Form->input('fax');
            echo $this->Form->input('cellphone');
            echo $this->Form->input('email');
            echo $this->Form->input('physical_address');
            echo $this->Form->input('postal_address');
            echo $this->Form->input('notes');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
