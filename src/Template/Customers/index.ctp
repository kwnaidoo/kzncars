
<div class="customers index large-12 medium-12 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('first_name') ?></th>
            <th><?= $this->Paginator->sort('surname') ?></th>
            <th><?= $this->Paginator->sort('telephone') ?></th>
            <th><?= $this->Paginator->sort('fax') ?></th>
            <th><?= $this->Paginator->sort('cellphone') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($customers as $customer): ?>
        <tr>
            <td><?= $this->Number->format($customer->id) ?></td>
            <td><?= h($customer->first_name) ?></td>
            <td><?= h($customer->surname) ?></td>
            <td><?= h($customer->telephone) ?></td>
            <td><?= h($customer->fax) ?></td>
            <td><?= h($customer->cellphone) ?></td>
            <td><?= h($customer->email) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customer->id]) ?><br />
                <?= $this->Html->link(__('Add Task'), ['controller'=>'customer_tasks', 'action' => 'add', $customer->id]) ?><br />
                <?= $this->Html->link(__("View Task's"), ['controller'=>'customer_tasks', 'action' => 'index', $customer->id]) ?>
                <br />
                  <?= $this->Html->link(__('Add Document'), ['controller'=>'customer_documents', 'action' => 'add', $customer->id]) ?><br />
                <?= $this->Html->link(__("View Documents"), ['controller'=>'customer_documents', 'action' => 'index', $customer->id]) ?>
             
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
