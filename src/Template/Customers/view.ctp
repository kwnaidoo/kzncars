
<div class="customers view large-12 medium-12 columns">
    <h2><?= h($customer->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('First Name') ?></h6>
            <p><?= h($customer->first_name) ?></p>
            <h6 class="subheader"><?= __('Surname') ?></h6>
            <p><?= h($customer->surname) ?></p>
            <h6 class="subheader"><?= __('Telephone') ?></h6>
            <p><?= h($customer->telephone) ?></p>
            <h6 class="subheader"><?= __('Fax') ?></h6>
            <p><?= h($customer->fax) ?></p>
            <h6 class="subheader"><?= __('Cellphone') ?></h6>
            <p><?= h($customer->cellphone) ?></p>
            <h6 class="subheader"><?= __('Email') ?></h6>
            <p><?= h($customer->email) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($customer->id) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Physical Address') ?></h6>
            <?= $this->Text->autoParagraph(h($customer->physical_address)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Postal Address') ?></h6>
            <?= $this->Text->autoParagraph(h($customer->postal_address)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Notes') ?></h6>
            <?= $this->Text->autoParagraph(h($customer->notes)) ?>
        </div>
    </div>
</div>
