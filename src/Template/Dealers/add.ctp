
<div class="dealers form large-12 medium-12 columns">
    <?= $this->Form->create($dealer) ?>
    <fieldset>
        <legend><?= __('Add Dealer') ?></legend>
        <?php
            echo $this->Form->input('company');
            echo $this->Form->input('company_profile');
            echo $this->Form->input('fax_number');
            echo $this->Form->input('telephone_number');
            echo $this->Form->input('operating_times');
            echo $this->Form->input('email_address');
            echo $this->Form->input('physical_address');
            echo $this->Form->input('postal_address');

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
