
<div class="dealers index large-12 medium-12 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('company') ?></th>
            <th><?= $this->Paginator->sort('fax_number') ?></th>
            <th><?= $this->Paginator->sort('telephone_number') ?></th>
            <th><?= $this->Paginator->sort('email_address') ?></th>
            <th><?= $this->Paginator->sort('physical_address') ?></th>
            <th><?= $this->Paginator->sort('postal_address') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dealers as $dealer): ?>
        <tr>
            <td><?= $this->Number->format($dealer->id) ?></td>
            <td><?= h($dealer->company) ?></td>
            <td><?= h($dealer->fax_number) ?></td>
            <td><?= h($dealer->telephone_number) ?></td>
            <td><?= h($dealer->email_address) ?></td>
            <td><?= h($dealer->physical_address) ?></td>
            <td><?= h($dealer->postal_address) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dealer->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dealer->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dealer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dealer->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
