
<div class="dealers view large-12 medium-12 columns">
    <h2><?= h($dealer->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Company') ?></h6>
            <p><?= h($dealer->company) ?></p>
            <h6 class="subheader"><?= __('Fax Number') ?></h6>
            <p><?= h($dealer->fax_number) ?></p>
            <h6 class="subheader"><?= __('Telephone Number') ?></h6>
            <p><?= h($dealer->telephone_number) ?></p>
            <h6 class="subheader"><?= __('Email Address') ?></h6>
            <p><?= h($dealer->email_address) ?></p>
            <h6 class="subheader"><?= __('Physical Address') ?></h6>
            <p><?= h($dealer->physical_address) ?></p>
            <h6 class="subheader"><?= __('Postal Address') ?></h6>
            <p><?= h($dealer->postal_address) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dealer->id) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Company Profile') ?></h6>
            <?= $this->Text->autoParagraph(h($dealer->company_profile)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Operating Times') ?></h6>
            <?= $this->Text->autoParagraph(h($dealer->operating_times)) ?>
        </div>
    </div>
</div>

