<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'KZN Cars - Dealership Manager';
function get_url($controller, $action){
print FULL_BASE_URL."$controller/$action";
}
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->html->css('jquery-ui/jquery-ui.structure.min.css');?>
    <?= $this->html->css('jquery-ui/jquery-ui.min.css');?>
    <?= $this->html->css('jquery-ui/jquery-ui.theme.min.css');?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
     <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('jquery-ui.min.js') ?>
     <?= $this->Html->css('select2.min.css') ?>
     <?= $this->Html->script('select2.full.min.js') ?>
     <?= $this->fetch('js_head');?>
    <script>
     $(function() {
       $( "#tabs" ).tabs();
        $("form select").select2();
     });

     </script>

        <style>
       .ui-tabs .ui-tabs-hide {display: none !important;}

    </style>
</head>
<body>
    <header>
        <div class="header-title">
            <span>Dealership Manager - <?= $this->fetch('title') ?></span>
        </div>
        <div class="header-help">
           <select onchange="GotoPage(this)">
           <option value="/" selected>Menu</option>
           <?php if($this->request->session()->check("User")):?>
           	<?php $session = $this->request->session();
           	$user_type = $session->read("User.user_type"); ?>
           <?php if($user_type == "super"):?>
           <option value="<?php get_url('dealers','add');?>">Add Dealer</option>
           <option value="<?php get_url('suppliers','index');?>">View Suppliers</option>
          <option value="<?php get_url('suppliers','add');?>">Add Supplier</option>
          <option value="<?php get_url('customers','add');?>">Add Customer</option>
          <option value="<?php get_url('customers','index');?>">View Customers</option>
           <option value="<?php get_url('dealers','index');?>">Dealer List</option>
           <option value="<?php get_url('cars','add');?>">Add Car</option>
           <option value="<?php get_url('cars', 'index');?>">View Cars</option>
            <option value="<?php get_url('leads','index');?>">View Leads</option>
           <option value="<?php get_url('leads','add');?>">Add Lead</option>
          <option value="<?php get_url('pages','add');?>">Add CMS Page</option>
          <option value="<?php get_url('pages','index');?>">View CMS Pages</option>
           <option value="<?php get_url('models','add');?>">Add Model</option>
           <option value="<?php get_url('models','index');?>">View Models</option>
           <option value="<?php get_url('makes','add');?>">Add Make</option>
            <option value="<?php get_url('makes','index');?>">View Makes</option>
           <option value="<?php get_url('showrooms','add');?>">Add Showroom</option>

            <option value="<?php get_url('showrooms','index');?>">View Showrooms</option>
           <option value="<?php get_url('suburbs','add');?>">Add Suburb</option>
           <option value="<?php get_url('suburbs','index');?>">View Suburbs</option>
           <option value="<?php get_url('users','add');?>">Add User</option>
           <option value="<?php get_url('users','index');?>">View Users</option>
       <?php endif;?>
              <?php if($user_type == "user"):?>
                  <option value="<?php get_url('cars','add');?>">Add Car</option>
                   <option value="<?php get_url('cars','index');?>">View Cars</option>
                    <option value="<?php get_url('leads','index');?>">View Leads</option>
                   <option value="<?php get_url('leads','add');?>">Add Lead</option>
                     <option value="<?php get_url('suppliers','index');?>">View Suppliers</option>
                    <option value="<?php get_url('suppliers','add');?>">Add Supplier</option>
                     <option value="<?php get_url('customers','add');?>">Add Customer</option>
          <option value="<?php get_url('customers','index');?>">View Customers</option>

              <?php endif;?>
              <?php if($user_type == "dealer"):?>

              <option value="<?php get_url('cars','add');?>">Add Car</option>
              <option value="<?php get_url('cars','index');?>">View Cars</option>
              <option value="<?php get_url('leads','index');?>">View Leads</option>
              <option value="<?php get_url('leads','add');?>">Add Lead</option>
              <option value="<?php get_url('pages','add');?>">Add CMS Page</option>
              <option value="<?php get_url('pages','index');?>">View CMS Pages</option>
              <option value="<?php get_url('showrooms','add');?>">Add Showroom</option>

               <option value="<?php get_url('showrooms','index');?>">View Showrooms</option>
              <option value="<?php get_url('suppliers','index');?>">View Suppliers</option>
          <option value="<?php get_url('suppliers','add');?>">Add Supplier</option>
           <option value="<?php get_url('customers','add');?>">Add Customer</option>
          <option value="<?php get_url('customers','index');?>">View Customers</option>
            
          	<?php endif;?>
              <option value="<?php get_url('users','logout');?>">Logout</option>
          <?php endif;?>


           </select>
    </header>
    <div id="container">

        <div id="content">
            <?= $this->Flash->render() ?>

            <div class="row">
                <?= $this->fetch('content') ?>
            </div>
        </div>
        <footer>
        </footer>
    </div>
    <script>
    function GotoPage(menu){
       window.location = menu.value;
    }
    </script>

</body>
</html>
