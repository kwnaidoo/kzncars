
<div class="leads form large-12 medium-12 columns">
    <?= $this->Form->create($lead) ?>
    <fieldset>
        <legend><?= __('Add Lead') ?></legend>
        <?php
            echo $this->Form->input('car_id', ['options' => $cars]);
            echo $this->Form->input('first_name');
            echo $this->Form->input('surname');
            echo $this->Form->input('cellphone');
            echo $this->Form->input('telephone');
            echo $this->Form->input('message');
            echo $this->Form->input('user_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
