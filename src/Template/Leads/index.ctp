
<div class="leads index large-12 medium-12 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('car_id') ?></th>
            <th><?= $this->Paginator->sort('first_name') ?></th>
            <th><?= $this->Paginator->sort('surname') ?></th>
            <th><?= $this->Paginator->sort('cellphone') ?></th>
            <th><?= $this->Paginator->sort('telephone') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($leads as $lead): ?>
        <tr>
            <td><?= $this->Number->format($lead->id) ?></td>
            <td>
                <?= $lead->has('car') ? $this->Html->link($lead->car->id, ['controller' => 'Cars', 'action' => 'view', $lead->car->id]) : '' ?>
            </td>
            <td><?= h($lead->first_name) ?></td>
            <td><?= h($lead->surname) ?></td>
            <td><?= h($lead->cellphone) ?></td>
            <td><?= h($lead->telephone) ?></td>
            <td><?= h($lead->created) ?></td>
            <td class="actions">
               
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $lead->id]) ?>
        
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
