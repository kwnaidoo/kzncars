
<div class="makes form large-12 medium-12 columns">
    <?= $this->Form->create($make) ?>
    <fieldset>
        <legend><?= __('Edit Make') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
