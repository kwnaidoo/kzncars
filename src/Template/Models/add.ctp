
<div class="models form large-12 medium-12 columns">
    <?= $this->Form->create($model) ?>
    <fieldset>
        <legend><?= __('Add Model') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('make_id', ['options' => $makes, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
