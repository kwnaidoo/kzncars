
<div class="pages form large-12 medium-12 columns">
    <?= $this->Form->create($page) ?>
    <fieldset>
        <legend><?= __('Add Page') ?></legend>
        <?php
            echo $this->Form->input('title');
            ?>
            <?php if($pages->count() >= 1){
            echo $this->Form->input('page_id', ['options' => $pages, "label" => "Parent Page"]);
            }
            ?>
            <?php
            echo $this->Form->input('content', ['id' => 'texteditor']);
            echo $this->Form->input('meta_keywords');
            echo $this->Form->input('meta_description');
            echo $this->Form->input('rank');
               echo $this->Form->input('show_on_website', ['options' => [1 => "Yes", 0 => "No"]]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<!-- ckeditor -->
<?= $this->Html->script('ckeditor/ckeditor.js', ['block' => 'js_head']);?>
 <script>
 CKEDITOR.replace('texteditor');
</script>