
<div class="pages index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('title') ?></th>
            <th><?= $this->Paginator->sort('page_id', "Parent Page") ?></th>
            <th><?= $this->Paginator->sort('rank') ?></th>
            <th><?= $this->Paginator->sort('show_on_website') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($pages as $page): ?>
        <tr>
            <td><?= $this->Number->format($page->id) ?></td>
            <td><?= h($page->title) ?></td>
            <td><?= h($page->page_id) ?></td>
            <td><?= h($page->rank) ?></td>
            <td><?= h($page->show_on_website) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $page->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $page->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $page->id], ['confirm' => __('Are you sure you want to delete # {0}?', $page->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
