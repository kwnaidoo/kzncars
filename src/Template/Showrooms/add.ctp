
<div class="showrooms form large-12 medium-12 columns">
    <?= $this->Form->create($showroom) ?>
    <fieldset>
        <legend><?= __('Add Showroom') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('address');
            echo $this->Form->input('telephone_number');
            echo $this->Form->input('fax_number');
            echo $this->Form->input('email_address');
            echo $this->Form->input('profile');
            echo $this->Form->input('dealer_id', ['options' => $dealers, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
