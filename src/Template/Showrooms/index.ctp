
<div class="showrooms index large-12 medium-12 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('telephone_number') ?></th>
            <th><?= $this->Paginator->sort('fax_number') ?></th>
            <th><?= $this->Paginator->sort('email_address') ?></th>
            <th><?= $this->Paginator->sort('dealer_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($showrooms as $showroom): ?>
        <tr>
            <td><?= $this->Number->format($showroom->id) ?></td>
            <td><?= h($showroom->name) ?></td>
            <td><?= h($showroom->telephone_number) ?></td>
            <td><?= h($showroom->fax_number) ?></td>
            <td><?= h($showroom->email_address) ?></td>
            <td>
                <?= $showroom->has('dealer') ? $this->Html->link($showroom->dealer->id, ['controller' => 'Dealers', 'action' => 'view', $showroom->dealer->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $showroom->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $showroom->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $showroom->id], ['confirm' => __('Are you sure you want to delete # {0}?', $showroom->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
