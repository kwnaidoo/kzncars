
<div class="showrooms view large-12 medium-12 columns">
    <h2><?= h($showroom->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($showroom->name) ?></p>
            <h6 class="subheader"><?= __('Telephone Number') ?></h6>
            <p><?= h($showroom->telephone_number) ?></p>
            <h6 class="subheader"><?= __('Fax Number') ?></h6>
            <p><?= h($showroom->fax_number) ?></p>
            <h6 class="subheader"><?= __('Email Address') ?></h6>
            <p><?= h($showroom->email_address) ?></p>
            <h6 class="subheader"><?= __('Dealer') ?></h6>
            <p><?= $showroom->has('dealer') ? $this->Html->link($showroom->dealer->id, ['controller' => 'Dealers', 'action' => 'view', $showroom->dealer->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($showroom->id) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Address') ?></h6>
            <?= $this->Text->autoParagraph(h($showroom->address)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Profile') ?></h6>
            <?= $this->Text->autoParagraph(h($showroom->profile)) ?>
        </div>
    </div>
</div>
