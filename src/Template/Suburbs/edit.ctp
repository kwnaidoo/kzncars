
<div class="suburbs form large-12 medium-12 columns">
    <?= $this->Form->create($suburb) ?>
    <fieldset>
        <legend><?= __('Edit Suburb') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
