
<div class="suppliers form large-12 medium-12 columns">
    <?= $this->Form->create($supplier) ?>
    <fieldset>
        <legend><?= __('Edit Supplier') ?></legend>
        <?php
            echo $this->Form->input('company');
            echo $this->Form->input('telephone');
            echo $this->Form->input('fax');
            echo $this->Form->input('contact_person');
            echo $this->Form->input('email');
            echo $this->Form->input('physical_address');
            echo $this->Form->input('postal_address');
            echo $this->Form->input('notes');
            echo $this->Form->input('vat_number');
            echo $this->Form->input('reg_number');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
