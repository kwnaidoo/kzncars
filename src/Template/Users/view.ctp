
<div class="users view large-12 medium-12 columns">
    <h2><?= h($user->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Username') ?></h6>
            <p><?= h($user->username) ?></p>
   
            <h6 class="subheader"><?= __('User Type') ?></h6>
            <p><?= h($user->user_type) ?></p>
            <h6 class="subheader"><?= __('Dealer') ?></h6>
            <p><?= $user->has('Dealers') ? $this->Html->link($user->dealer->id, ['controller' => 'Dealers', 'action' => 'view', $user->dealer->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($user->id) ?></p>
        </div>
    </div>
</div>
