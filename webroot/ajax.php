<?php 
require_once("../front/controller.php");
$service = "";
$url = $_SERVER['REQUEST_URI'];
if(isset($_GET['service'])){
	$service = $_GET['service'];
}
elseif(strpos($url, "get_models") !== false){
	print get_models_by_make();
}

switch($service){

	case "get_models":
	    $make_id = isset($_GET['make_id']) ? $_GET['make_id'] : null;
	    get_models_by_make($make_id);
}

