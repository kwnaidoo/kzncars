<?php 
require_once("../front/controller.php");
$url = $_SERVER['REQUEST_URI'];
if(strpos($url, "search") !== false){
	$cars = get_cars();
	get_layout("header");
	require_once(VIEW_PATH."search_results.php");
	get_layout("footer");
}

else{
	get_layout("header");
	require_once(VIEW_PATH."home.php");
	get_layout("footer");
}

